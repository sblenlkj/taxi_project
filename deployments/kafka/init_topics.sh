#!/bin/bash

kafka-topics --create --if-not-exists --topic $KAFKA_TRIP_INBOUND_TOPIC_NAME --bootstrap-server kafka:9092
kafka-topics --create --if-not-exists --topic $KAFKA_TRIP_OUTBOUND_TOPIC_NAME --bootstrap-server kafka:9092
