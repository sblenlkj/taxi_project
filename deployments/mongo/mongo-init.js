db.createCollection(process.env.MONGO_DRIVERS_COLLECTION_NAME)
//Test drivers
db.drivers.insertMany(
    [
        {
            "user_id": UUID("d4e111c4-a570-4cc0-afa5-7937b1d757b5"),
            "is_busy": false,
        },
        {
            "user_id": UUID("d9e70a75-9a92-4147-b09d-b9f97160bf62"),
            "is_busy": false,
        },
        {
            "user_id": UUID("1fd7d876-0acb-4cbd-8dce-8696f12f6217"),
            "is_busy": false,
        },
    ]
)
db.drivers.createIndex({ "user_id": 1 })

db.createCollection(process.env.MONGO_ASSIGNMENTS_COLLECTION_NAME)
db.assignments.createIndex({ "trip_id": 1 })

db.createCollection(process.env.MONGO_TRIPS_COLLECTION_NAME)
db.trips.createIndex({ "trip_id": 1 })