FROM alpine

WORKDIR /app

COPY --from=driver_service_build:develop /app/cmd/driver/main ./app

CMD ["/app/app"]