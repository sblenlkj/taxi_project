package api

import (
	"context"
	"driver/internal/app/driver/api/codegen"
	"driver/internal/app/driver/config"
	"driver/internal/app/driver/service"
	models "driver/internal/pkg/models"
	"errors"
)

type Server struct {
	driverService *service.Service
}

func NewServer(ctx context.Context, cfg *config.Config) (*Server, error) {
	driverService, err := service.NewService(ctx, cfg)
	if err != nil {
		return nil, err
	}
	return &Server{
		driverService: driverService,
	}, nil
}

// (GET /trips)
func (s *Server) GetTrips(ctx context.Context, request codegen.GetTripsRequestObject) (codegen.GetTripsResponseObject, error) {
	trips, err := s.driverService.GetTrips(ctx, request.Params.UserId)

	if errors.Is(err, models.ErrDriverNotFound) {
		return codegen.GetTrips403JSONResponse{
			Code: "unathorized",
		}, nil
	}
	if errors.Is(err, models.ErrDriverOnTrip) {
		return codegen.GetTrips403JSONResponse{
			Code: "already_on_trip",
		}, nil
	}
	if err != nil {
		return nil, err
	}

	result := make([]codegen.Trip, len(trips))
	for i, trip := range trips {
		result[i] = ConvertToApiType(trip)
	}
	return codegen.GetTrips200JSONResponse(result), nil
}

// (POST /trips/{trip_id}/accept)
func (s *Server) PostTripsTripIdAccept(ctx context.Context, request codegen.PostTripsTripIdAcceptRequestObject) (codegen.PostTripsTripIdAcceptResponseObject, error) {
	err := s.driverService.AcceptTrip(ctx, request.TripId, request.Params.UserId)
	if errors.Is(err, models.ErrDriverNotFound) {
		return codegen.PostTripsTripIdAccept403JSONResponse{
			Code: "unathorized",
		}, nil
	}
	if errors.Is(err, models.ErrCantAccept) ||
		errors.Is(err, models.ErrTripNotFound) {
		// Return 404 if driver is not allowed to accept this trip
		return codegen.PostTripsTripIdAccept404Response{}, nil
	}
	if errors.Is(err, models.ErrDriverOnTrip) {
		return codegen.PostTripsTripIdAccept409JSONResponse{
			Code: "already_on_trip",
		}, nil
	}
	if err != nil {
		return nil, err
	}

	return codegen.PostTripsTripIdAccept200Response{}, nil
}

// (POST /trips/{trip_id}/cancel)
func (s *Server) PostTripsTripIdCancel(ctx context.Context, request codegen.PostTripsTripIdCancelRequestObject) (codegen.PostTripsTripIdCancelResponseObject, error) {
	err := s.driverService.CancelTripByDriver(ctx, request.TripId, request.Params.UserId, request.Params.Reason)

	if errors.Is(err, models.ErrDriverNotFound) {
		return codegen.PostTripsTripIdCancel403JSONResponse{Code: "unathorized"}, nil
	}
	if errors.Is(err, models.ErrTripNotFound) {
		return codegen.PostTripsTripIdCancel404Response{}, nil
	}
	if errors.Is(err, models.ErrCantCancel) {
		msg := "can't cancel trip in current status"
		return codegen.PostTripsTripIdCancel409JSONResponse{
			Code:    "cant_cancel",
			Message: &msg,
		}, nil
	}
	if err != nil {
		return nil, err
	}

	return codegen.PostTripsTripIdCancel200Response{}, nil
}

// (POST /trips/{trip_id}/end)
func (s *Server) PostTripsTripIdEnd(ctx context.Context, request codegen.PostTripsTripIdEndRequestObject) (codegen.PostTripsTripIdEndResponseObject, error) {
	err := s.driverService.EndTrip(ctx, request.TripId, request.Params.UserId)

	if errors.Is(err, models.ErrDriverNotFound) {
		return codegen.PostTripsTripIdEnd403JSONResponse{Code: "unathorized"}, nil
	}
	if errors.Is(err, models.ErrTripNotFound) {
		return codegen.PostTripsTripIdEnd404Response{}, nil
	}
	if errors.Is(err, models.ErrCantEnd) {
		msg := "can't end trip in current status"
		return codegen.PostTripsTripIdEnd409JSONResponse{
			Code:    "cant_end",
			Message: &msg,
		}, nil
	}
	if err != nil {
		return nil, err
	}

	return codegen.PostTripsTripIdEnd200Response{}, nil
}

// (POST /trips/{trip_id}/start)
func (s *Server) PostTripsTripIdStart(ctx context.Context, request codegen.PostTripsTripIdStartRequestObject) (codegen.PostTripsTripIdStartResponseObject, error) {
	err := s.driverService.StartTrip(ctx, request.TripId, request.Params.UserId)

	if errors.Is(err, models.ErrDriverNotFound) {
		return codegen.PostTripsTripIdStart403JSONResponse{Code: "unathorized"}, nil
	}
	if errors.Is(err, models.ErrTripNotFound) {
		return codegen.PostTripsTripIdStart404Response{}, nil
	}
	if errors.Is(err, models.ErrCantStart) {
		msg := "can't start trip in current status"
		return codegen.PostTripsTripIdStart409JSONResponse{
			Code:    "cant_start",
			Message: &msg,
		}, nil
	}
	if err != nil {
		return nil, err
	}

	return codegen.PostTripsTripIdStart200Response{}, nil
}

func (s *Server) GetTripsTripId(ctx context.Context, request codegen.GetTripsTripIdRequestObject) (codegen.GetTripsTripIdResponseObject, error) {
	trip, err := s.driverService.GetCurrentTripInfo(ctx, request.TripId, request.Params.UserId)
	if errors.Is(err, models.ErrDriverNotFound) {
		return codegen.GetTripsTripId403JSONResponse{Code: "unathorized"}, nil
	}
	if errors.Is(err, models.ErrTripNotFound) {
		return codegen.GetTripsTripId404Response{}, nil
	}
	return codegen.GetTripsTripId200JSONResponse(ConvertToApiType(*trip)), nil
}

func (s *Server) Start(ctx context.Context) {
	s.driverService.Start(ctx)
}

func (s *Server) Stop(ctx context.Context) {
	s.driverService.Stop(ctx)
}
