package api

import (
	"driver/internal/app/driver/api/codegen"
	models "driver/internal/pkg/models"
)

func ConvertToApiType(trip models.Trip) codegen.Trip {
	return codegen.Trip{
		Id:       trip.Id,
		From:     trip.From,
		To:       trip.To,
		Price:    trip.Price,
		Status:   trip.Status,
		DriverId: trip.DriverId,
	}
}
