package app

import (
	"context"
	"driver/internal/app/driver/api"
	"driver/internal/app/driver/api/codegen"
	"driver/internal/app/driver/config"
	"driver/internal/app/driver/logger"
	"driver/internal/app/driver/otel"
	"errors"
	"fmt"
	"net"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/juju/zaputil/zapctx"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
)

type App struct {
	apiServer                *api.Server
	cfg                      *config.Config
	httpServer               *http.Server
	logger                   *zap.Logger
	metricsServer            *http.Server
	otelProviderShutdownFunc func()
}

func NewApp() *App {
	return &App{}
}

func (a *App) Init(ctx context.Context, cfg *config.Config) error {
	a.cfg = cfg

	apiServer, err := api.NewServer(ctx, cfg)
	if err != nil {
		return err
	}
	a.apiServer = apiServer

	logger, err := logger.GetLogger(true)
	if err != nil {
		return err
	}
	a.logger = logger

	a.httpServer = newHttpServer(a.apiServer, a.logger, a.cfg)

	logger.Info("Setup http server for metrics")
	metricsRouter := chi.NewRouter()
	metricsRouter.Handle("/metrics", promhttp.Handler())
	metrics_server := &http.Server{Addr: ":9000", Handler: metricsRouter}
	a.metricsServer = metrics_server

	logger.Info("Init otel provider")
	a.otelProviderShutdownFunc = otel.InitProvider()

	return nil
}

func newHttpServer(apiServer codegen.StrictServerInterface, logger *zap.Logger, cfg *config.Config) *http.Server {
	handler_opts := codegen.StrictHTTPServerOptions{
		RequestErrorHandlerFunc: func(w http.ResponseWriter, r *http.Request, err error) {
			http.Error(w, err.Error(), http.StatusBadRequest)
		},
		ResponseErrorHandlerFunc: func(w http.ResponseWriter, r *http.Request, err error) {
			logger.Error(err.Error())
			http.Error(w, "server got itself in trouble", http.StatusInternalServerError)
		},
	}
	strict_handler := codegen.NewStrictHandlerWithOptions(apiServer, make([]codegen.StrictMiddlewareFunc, 0), handler_opts)
	options := codegen.ChiServerOptions{
		BaseURL:     "",
		BaseRouter:  chi.NewRouter(),
		Middlewares: make([]codegen.MiddlewareFunc, 0),
	}

	return &http.Server{
		Handler: codegen.HandlerWithOptions(strict_handler, options),
		Addr:    fmt.Sprintf(":%s", cfg.Http.Port),
		BaseContext: func(_ net.Listener) context.Context {
			return zapctx.WithLogger(context.Background(), logger)
		},
	}
}

func (a *App) Start(ctx context.Context) error {
	ctx = zapctx.WithLogger(ctx, a.logger)
	go func() {
		a.apiServer.Start(ctx)
		go a.metricsServer.ListenAndServe()
		if err := a.httpServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			a.logger.Sugar().Fatalf("Could not listen on port %s: %s", a.cfg.Http.Port, err.Error())
		}
	}()

	a.logger.Sugar().Infof(
		"Started serving driver service at port %s",
		a.cfg.Http.Port,
	)

	return nil
}

func (a *App) Stop(ctx context.Context) error {
	<-ctx.Done()

	done := make(chan bool)
	a.logger.Info("Server is shutting down...")

	go func() {
		if err := a.httpServer.Shutdown(context.Background()); err != nil {
			a.logger.Sugar().Fatal("Could not gracefully shutdown main server: ", err.Error())
		}
		if err := a.metricsServer.Shutdown(context.Background()); err != nil {
			a.logger.Sugar().Fatal("Could not gracefully shutdown metrics server: ", err.Error())
		}
		a.otelProviderShutdownFunc()
		a.apiServer.Stop(ctx)
		close(done)
	}()

	<-done
	return nil
}
