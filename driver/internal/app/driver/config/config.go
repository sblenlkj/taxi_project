package config

import (
	"log"
	"os"
	"strconv"
	"time"
)

type Http struct {
	Port string
}

type Mongo struct {
	Uri                       string
	Username                  string
	Password                  string
	AuthSource                string
	DbName                    string
	DriversCollectionName     string
	AssignmentsCollectionName string
	TripsCollectionName       string
}

type Kafka struct {
	BrokerAddr            string
	TripInboundTopicName  string
	TripOutboundTopicName string
}

type Dispatch struct {
	Period time.Duration
}

type ClientAddrs struct {
	Location string
}

type Config struct {
	Http        Http
	Mongo       Mongo
	Kafka       Kafka
	Dispatch    Dispatch
	ClientAddrs ClientAddrs
}

func getEnv(key string, default_value string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return default_value
}

func getHttpConfig() Http {
	return Http{
		Port: getEnv("DRIVER_SERVICE_PORT", "80"),
	}
}

func getMongoConfig() Mongo {
	return Mongo{
		Uri:                       getEnv("DRIVER_SERVICE_MONGO_URI", "mongo"),
		Username:                  getEnv("DRIVER_SERVICE_MONGO_USERNAME", "user"),
		Password:                  getEnv("DRIVER_SERVICE_MONGO_PASSWORD", "password"),
		AuthSource:                getEnv("DRIVER_SERVICE_MONGO_AUTH_SOURCE", "admin"),
		DbName:                    getEnv("DRIVER_SERVICE_MONGO_DB_NAME", "driver"),
		DriversCollectionName:     getEnv("DRIVER_SERVICE_MONGO_DRIVERS_COLLECTION_NAME", "drivers"),
		AssignmentsCollectionName: getEnv("DRIVER_SERVICE_MONGO_ASSIGNMENTS_COLLECTION_NAME", "assignments"),
		TripsCollectionName:       getEnv("DRIVER_SERVICE_MONGO_TRIPS_COLLECTION_NAME", "trips"),
	}
}

func getKafkaConfig() Kafka {
	return Kafka{
		BrokerAddr:            getEnv("DRIVER_SERVICE_KAFKA_BROKER_ADDRESS", "kafka:29092"),
		TripInboundTopicName:  getEnv("DRIVER_SERVICE_KAFKA_TRIP_INBOUND_TOPIC_NAME", "trip inbound"),
		TripOutboundTopicName: getEnv("DRIVER_SERVICE_KAFKA_TRIP_OUTBOUND_TOPIC_NAME", "trip outbound"),
	}
}

func getDispatchConfig() Dispatch {
	period_ms, err := strconv.Atoi(getEnv("DRIVER_SERVICE_DISPATCH_PERIOD_MS", "5000"))
	if err != nil {
		log.Fatal(err)
	}
	return Dispatch{
		Period: time.Millisecond * time.Duration(period_ms),
	}
}

func getClientAddrsConfig() ClientAddrs {
	return ClientAddrs{
		Location: getEnv("DRIVER_SERVICE_LOCATION_SERVER_ADDR", "location"),
	}
}

func GetConfig() *Config {
	return &Config{
		Http:        getHttpConfig(),
		Mongo:       getMongoConfig(),
		Kafka:       getKafkaConfig(),
		Dispatch:    getDispatchConfig(),
		ClientAddrs: getClientAddrsConfig(),
	}
}
