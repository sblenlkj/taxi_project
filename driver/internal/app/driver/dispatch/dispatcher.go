package dispatch

import (
	"context"
	location "driver/internal/app/driver/api/codegen/clients"
	"driver/internal/app/driver/tracer"
	models "driver/internal/pkg/models"
	"encoding/json"
	"strconv"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/juju/zaputil/zapctx"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type metrics struct {
	tripsWithoutDriver prometheus.Gauge
	driversWithoutTrip prometheus.Gauge
}

type Dispatcher struct {
	tripsMut sync.RWMutex
	trips    map[uuid.UUID]*models.Trip

	driversMut sync.RWMutex
	drivers    map[uuid.UUID]struct{}

	dispatchStepMut sync.Mutex

	locationClient location.ClientWithResponsesInterface

	metrics metrics

	// TODO: возможно диспатчу нужно самому вычитывать
	// коллекцию assignments, чтобы не предлагать поездку
	// одному и тому же водителю несколько раз
}

func NewDispatcher(locationClient location.ClientWithResponsesInterface) *Dispatcher {
	tripsWithoutDriver := promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: "driver", Name: "trips_without_driver",
	})
	driversWithoutTrip := promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: "driver", Name: "drivers_without_trip",
	})

	return &Dispatcher{
		trips:          make(map[uuid.UUID]*models.Trip),
		drivers:        make(map[uuid.UUID]struct{}),
		locationClient: locationClient,
		metrics: metrics{
			tripsWithoutDriver: tripsWithoutDriver,
			driversWithoutTrip: driversWithoutTrip,
		},
	}
}

func (d *Dispatcher) NewTrip(trip *models.Trip) {
	d.tripsMut.Lock()
	defer d.tripsMut.Unlock()

	d.trips[trip.Id] = trip

	d.metrics.tripsWithoutDriver.Set(float64(len(d.trips)))
}

func (d *Dispatcher) RemoveTrip(tripId uuid.UUID) {
	d.tripsMut.Lock()
	defer d.tripsMut.Unlock()

	delete(d.trips, tripId)

	d.metrics.tripsWithoutDriver.Set(float64(len(d.trips)))
}

func (d *Dispatcher) NewDriver(driverId uuid.UUID) {
	d.driversMut.Lock()
	defer d.driversMut.Unlock()

	d.drivers[driverId] = struct{}{}

	d.metrics.driversWithoutTrip.Set(float64(len(d.drivers)))
}

func (d *Dispatcher) RemoveDriver(driverId uuid.UUID) {
	d.driversMut.Lock()
	defer d.driversMut.Unlock()

	delete(d.drivers, driverId)

	d.metrics.driversWithoutTrip.Set(float64(len(d.drivers)))
}

func (d *Dispatcher) StartDispatching(ctx context.Context, period time.Duration, results chan models.DispatchResult) {
	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	go func() {
		ticker := time.NewTicker(period)
		var totalSteps uint64 = 0
		for {
			select {
			case <-ticker.C:
				if !d.dispatchStepMut.TryLock() {
					logger.Info("Last dispatch step is not finished, skip running a new one")
					continue
				}
				totalSteps += 1
				func() {
					defer d.dispatchStepMut.Unlock()
					d.makeOneDispatchStep(ctx, results, totalSteps)
				}()
			case <-ctx.Done():
				return
			}
		}
	}()
}

func makeCopy[K comparable, V any](origin map[K]V) map[K]V {
	copy := make(map[K]V)
	for k, v := range origin {
		copy[k] = v
	}
	return copy
}

func (d *Dispatcher) getCopyOfFreeDrivers() map[uuid.UUID]struct{} {
	d.driversMut.RLock()
	defer d.driversMut.RUnlock()
	return makeCopy(d.drivers)
}

func (d *Dispatcher) getCopyOfTripsWithoutDriver() map[uuid.UUID]*models.Trip {
	d.tripsMut.RLock()
	defer d.tripsMut.RUnlock()
	return makeCopy(d.trips)
}

func (d *Dispatcher) makeOneDispatchStep(ctx context.Context, results chan models.DispatchResult, stepIndx uint64) {
	ctx, span := tracer.Tracer.Start(ctx, "dispatchStep_"+strconv.FormatUint(stepIndx, 10))
	defer span.End()

	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	logger.Sugar().Infof("Start dispatch step %d", stepIndx)

	drivers := d.getCopyOfFreeDrivers()
	if len(drivers) == 0 {
		return
	}

	trips := d.getCopyOfTripsWithoutDriver()
	if len(trips) == 0 {
		return
	}

	for _, trip := range trips {
		logger.Sugar().Infof("Trying to find candidates for trip %s", trip.Id)

		// TODO: make radius configurable
		resp, err := d.locationClient.GetDriversWithResponse(ctx, &location.GetDriversParams{Lat: trip.From.Lat, Lng: trip.From.Lng, Radius: 5})
		if err != nil {
			logger.Sugar().Errorf("Error on getting drivers from location: %s", err.Error())
			continue
		}
		if resp.StatusCode() != 200 {
			logger.Sugar().Errorf("Error on getting drivers from location. status_code=%s, body=%s", resp.StatusCode(), string(resp.Body))
			continue
		}

		var driversInCircle []location.Driver
		err = json.Unmarshal(resp.Body, &driversInCircle)
		if err != nil {
			logger.Sugar().Errorf("Error on unmarshalling drivers Get /drivers/ response: %s", err.Error())
			continue
		}

		for _, driver := range driversInCircle {
			_, exists := drivers[driver.Id]
			if !exists {
				continue
			}
			// TODO: по-хорошему, тут надо смотреть,
			// что этому кандидату эту поездку
			// ещё не предлагали.
			results <- models.DispatchResult{
				Trip:     trip,
				DriverId: driver.Id,
			}
		}
	}
}
