package polling

import (
	models "driver/internal/pkg/models"
	"sync"

	"github.com/google/uuid"
)

type tripsPolling struct {
	mut       sync.Mutex
	container map[uuid.UUID]*chan *models.Trip
}

func NewTripsPolling() *tripsPolling {
	return &tripsPolling{
		container: make(map[uuid.UUID]*chan *models.Trip),
	}
}

func (p *tripsPolling) AddDriver(driverId uuid.UUID, result *chan *models.Trip) {
	p.mut.Lock()
	defer p.mut.Unlock()

	p.container[driverId] = result
}

func (p *tripsPolling) GetDriversChan(driverId uuid.UUID) (*chan *models.Trip, error) {
	p.mut.Lock()
	defer p.mut.Unlock()

	ch, exists := p.container[driverId]
	if exists {
		return ch, nil
	}
	return nil, models.ErrDriverNotFound
}

func (p *tripsPolling) RemoveDriver(driverId uuid.UUID) {
	p.mut.Lock()
	defer p.mut.Unlock()

	delete(p.container, driverId)
}
