package repository

import (
	"context"

	"driver/internal/app/driver/tracer"
	models "driver/internal/pkg/models"
	"errors"

	trmsql "github.com/avito-tech/go-transaction-manager/sql"
	"github.com/google/uuid"
	"github.com/juju/zaputil/zapctx"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type assignmentsRepository struct {
	collection *mongo.Collection
	ctxGetter  *trmsql.CtxGetter
}

func newAssignmentsRepository(collection *mongo.Collection, ctxGetter *trmsql.CtxGetter) *assignmentsRepository {
	return &assignmentsRepository{
		collection: collection,
		ctxGetter:  ctxGetter,
	}
}

func (r *assignmentsRepository) Create(ctx context.Context, assignment *models.Assignment) error {
	ctx, span := tracer.Tracer.Start(ctx, "assignmentsRepo.Create")
	defer span.End()

	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	_, err := r.collection.InsertOne(ctx, assignment)
	return err
}

func (r *assignmentsRepository) Get(ctx context.Context, tripId uuid.UUID) (*models.Assignment, error) {
	ctx, span := tracer.Tracer.Start(ctx, "assignmentsRepo.Get")
	defer span.End()

	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	res := r.collection.FindOne(ctx, bson.M{"trip_id": tripId})
	err := res.Err()

	if errors.Is(err, mongo.ErrNoDocuments) {
		return nil, models.ErrAssignmentNotFound
	}
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	var assignment models.Assignment
	err = res.Decode(&assignment)
	if err != nil {
		return nil, err
	}
	return &assignment, nil
}

func (r *assignmentsRepository) AddCandidate(ctx context.Context, tripId uuid.UUID, candidate uuid.UUID) error {
	ctx, span := tracer.Tracer.Start(ctx, "assignmentsRepo.AddCandidate")
	defer span.End()

	res, err := r.collection.UpdateOne(ctx, bson.M{"trip_id": tripId},
		bson.M{"$push": bson.M{"candidate_ids": candidate}})
	if err != nil {
		return err
	}

	if res.MatchedCount != 1 {
		return models.ErrAssignmentNotFound
	}
	return nil
}

func (r *assignmentsRepository) AssignTo(ctx context.Context, tripId uuid.UUID, driverId uuid.UUID) error {
	ctx, span := tracer.Tracer.Start(ctx, "assignmentsRepo.AssignTo")
	defer span.End()

	res, err := r.collection.UpdateOne(ctx, bson.M{"trip_id": tripId},
		bson.M{"$set": bson.M{"need_to_assign": false, "assigned_to_id": driverId}})
	if err != nil {
		return err
	}

	if res.MatchedCount != 1 {
		return models.ErrAssignmentNotFound
	}
	return nil
}

func (r *assignmentsRepository) Stop(ctx context.Context, tripId uuid.UUID) error {
	ctx, span := tracer.Tracer.Start(ctx, "assignmentsRepo.Stop")
	defer span.End()

	res, err := r.collection.UpdateOne(ctx, bson.M{"trip_id": tripId},
		bson.M{"$set": bson.M{"need_to_assign": false}})
	if err != nil {
		return err
	}

	if res.MatchedCount != 1 {
		return models.ErrAssignmentNotFound
	}
	return nil
}
