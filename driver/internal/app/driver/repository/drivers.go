package repository

import (
	"context"
	"driver/internal/app/driver/tracer"
	models "driver/internal/pkg/models"
	"errors"

	"github.com/google/uuid"
	"github.com/juju/zaputil/zapctx"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	trmsql "github.com/avito-tech/go-transaction-manager/sql"
)

type driversRepository struct {
	collection *mongo.Collection
	ctxGetter  *trmsql.CtxGetter
}

func newDriversRepository(collection *mongo.Collection, ctxGetter *trmsql.CtxGetter) *driversRepository {
	return &driversRepository{
		collection: collection,
		ctxGetter:  ctxGetter,
	}
}

func (r *driversRepository) Get(ctx context.Context, id uuid.UUID) (*models.Driver, error) {
	ctx, span := tracer.Tracer.Start(ctx, "driversRepo.Get")
	defer span.End()

	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	res := r.collection.FindOne(ctx, bson.M{"user_id": id})
	err := res.Err()

	if errors.Is(err, mongo.ErrNoDocuments) {
		return nil, models.ErrDriverNotFound
	}
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	var driver models.Driver
	err = res.Decode(&driver)
	if err != nil {
		return nil, err
	}
	return &driver, nil
}

func (r *driversRepository) ChangeStatus(ctx context.Context, id uuid.UUID, isBusy bool, curTripId *uuid.UUID) error {
	ctx, span := tracer.Tracer.Start(ctx, "driversRepo.ChangeStatus")
	defer span.End()

	res, err := r.collection.UpdateOne(ctx, bson.M{"user_id": id},
		bson.M{"$set": bson.M{"is_busy": isBusy, "cur_trip_id": curTripId}})
	if err != nil {
		return err
	}

	if res.MatchedCount != 1 {
		return models.ErrDriverNotFound
	}
	return nil
}
