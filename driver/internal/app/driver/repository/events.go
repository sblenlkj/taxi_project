package repository

import (
	"context"
	"driver/internal/app/driver/config"
	"driver/internal/pkg/commands"
	"driver/internal/pkg/events"
	"errors"
	"time"

	"github.com/juju/zaputil/zapctx"
	"github.com/segmentio/kafka-go"
)

type eventsRepository struct {
	cfg    config.Kafka
	reader *kafka.Reader
	writer *kafka.Writer
}

func newEventsRepository(cfg config.Kafka) *eventsRepository {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:        []string{cfg.BrokerAddr},
		Topic:          cfg.TripInboundTopicName,
		SessionTimeout: time.Second * 5,
	})

	writer := kafka.NewWriter(kafka.WriterConfig{
		Brokers:   []string{cfg.BrokerAddr},
		Topic:     cfg.TripOutboundTopicName,
		Async:     false,
		BatchSize: 0,
	})

	return &eventsRepository{
		cfg:    cfg,
		reader: reader,
		writer: writer,
	}
}

func (r *eventsRepository) StartListeningToTripInboundEvents(ctx context.Context, callback func(ctx context.Context, event interface{})) error {
	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	logger.Sugar().Infof("Start listening to kafka topic %s", r.cfg.TripInboundTopicName)

	for {
		msg, err := r.reader.ReadMessage(ctx)

		if errors.Is(err, context.Canceled) {
			return nil
		}
		if err != nil {
			logger.Sugar().Errorf("Got error on reading message from kafka topic %s: %s", r.cfg.TripInboundTopicName, err.Error())
			return err
		}
		logger.Sugar().Debugf("new message from kafka topic %s", msg.Topic)

		event, err := events.ParseEvent(msg.Value)
		if err != nil {
			logger.Sugar().Errorf("Couldn't parse kafka event. Raw event: %s, error: %s", msg.Value, err)
			continue
		}
		callback(ctx, event)
	}
}

func (r *eventsRepository) PushCommand(ctx context.Context, command commands.Command) error {
	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	raw, err := command.MarshalJson()
	if err != nil {
		return err
	}

	return r.writer.WriteMessages(ctx, kafka.Message{
		Key:   command.Key(),
		Value: raw,
	})
}

func (r *eventsRepository) Close() error {
	err := r.reader.Close()
	if err != nil {
		return err
	}
	return r.writer.Close()
}
