package repository

import (
	"context"
	"driver/internal/app/driver/config"

	trmsql "github.com/avito-tech/go-transaction-manager/sql"
)

// TODO: в репозиториях не руками задавать имена полей,
// а доставать их из тегов полей

func GetRepositories(ctx context.Context, cfg *config.Config) (*driversRepository, *tripsRepository, *assignmentsRepository, *eventsRepository, interface{}, error) {
	client, err := getMongoClient(ctx, cfg.Mongo)
	if err != nil {
		return nil, nil, nil, nil, nil, err
	}

	driversCol := client.Database(cfg.Mongo.DbName).Collection(cfg.Mongo.DriversCollectionName)
	driversRepo := newDriversRepository(driversCol, trmsql.DefaultCtxGetter)

	tripsCol := client.Database(cfg.Mongo.DbName).Collection(cfg.Mongo.TripsCollectionName)
	tripsRepo := newTripsRepository(tripsCol, trmsql.DefaultCtxGetter)

	assignmentsCol := client.Database(cfg.Mongo.DbName).Collection(cfg.Mongo.AssignmentsCollectionName)
	assignmentsRepo := newAssignmentsRepository(assignmentsCol, trmsql.DefaultCtxGetter)

	eventsRepo := newEventsRepository(cfg.Kafka)

	return driversRepo, tripsRepo, assignmentsRepo, eventsRepo, client, nil
}
