package repository

import (
	"context"
	"errors"

	trmmongo "github.com/avito-tech/go-transaction-manager/mongo"
	"github.com/avito-tech/go-transaction-manager/trm/manager"
	"go.mongodb.org/mongo-driver/mongo"
)

type TransactionManager struct {
	manager *manager.Manager
}

var ErrUnexpectedClient = errors.New("expected mongo client")

func NewTransactionManager(client interface{}) (*TransactionManager, error) {
	switch c := client.(type) {
	case *mongo.Client:
		return &TransactionManager{manager: manager.Must(trmmongo.NewDefaultFactory(c))}, nil
	default:
		return nil, ErrUnexpectedClient
	}
}

func (tm *TransactionManager) WithTransaction(ctx context.Context, f func() error) error {
	return tm.manager.Do(ctx, func(ctx context.Context) error {
		return f()
	})
}
