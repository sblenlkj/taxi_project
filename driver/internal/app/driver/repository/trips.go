package repository

import (
	"context"
	"driver/internal/app/driver/api/codegen"
	"driver/internal/app/driver/tracer"
	models "driver/internal/pkg/models"
	"errors"

	trmsql "github.com/avito-tech/go-transaction-manager/sql"
	"github.com/google/uuid"
	"github.com/juju/zaputil/zapctx"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type tripsRepository struct {
	collection *mongo.Collection
	ctxGetter  *trmsql.CtxGetter
}

func newTripsRepository(collection *mongo.Collection, ctxGetter *trmsql.CtxGetter) *tripsRepository {
	return &tripsRepository{
		collection: collection,
		ctxGetter:  ctxGetter,
	}
}

func (r *tripsRepository) Create(ctx context.Context, trip *models.Trip) error {
	ctx, span := tracer.Tracer.Start(ctx, "tripsRepo.Create")
	defer span.End()

	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	_, err := r.collection.InsertOne(ctx, trip)
	if err != nil {
		return err
	}
	return nil
}

func (r *tripsRepository) Get(ctx context.Context, id uuid.UUID) (*models.Trip, error) {
	ctx, span := tracer.Tracer.Start(ctx, "tripsRepo.Get")
	defer span.End()

	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	res := r.collection.FindOne(ctx, bson.M{"id": id})
	err := res.Err()

	if errors.Is(err, mongo.ErrNoDocuments) {
		return nil, models.ErrTripNotFound
	}
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	var trip models.Trip
	err = res.Decode(&trip)
	if err != nil {
		return nil, err
	}
	return &trip, nil
}

func (r *tripsRepository) Remove(ctx context.Context, id uuid.UUID) error {
	ctx, span := tracer.Tracer.Start(ctx, "tripsRepo.Remove")
	defer span.End()

	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	res, err := r.collection.DeleteOne(ctx, bson.M{"id": id})
	if err != nil {
		logger.Error(err.Error())
		return err
	}
	if res.DeletedCount != 1 {
		return models.ErrTripNotFound
	}
	return nil
}

func (r *tripsRepository) ChangeStatus(ctx context.Context, id uuid.UUID, status codegen.TripStatus) error {
	ctx, span := tracer.Tracer.Start(ctx, "tripsRepo.ChangeStatus")
	defer span.End()

	res, err := r.collection.UpdateOne(ctx, bson.M{"id": id},
		bson.M{"$set": bson.M{"status": status}})
	if err != nil {
		return err
	}

	if res.MatchedCount != 1 {
		return models.ErrTripNotFound
	}
	return nil
}

func (r *tripsRepository) SetDriverId(ctx context.Context, id uuid.UUID, driverId *uuid.UUID) error {
	ctx, span := tracer.Tracer.Start(ctx, "tripsRepo.SetDriverId")
	defer span.End()

	res, err := r.collection.UpdateOne(ctx, bson.M{"id": id},
		bson.M{"$set": bson.M{"driver_id": driverId}})
	if err != nil {
		return err
	}

	if res.MatchedCount != 1 {
		return models.ErrTripNotFound
	}
	return nil
}
