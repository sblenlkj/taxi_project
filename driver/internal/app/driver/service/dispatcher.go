package service

import (
	"context"
	models "driver/internal/pkg/models"
	"time"

	"github.com/google/uuid"
)

type dispatcher interface {
	NewTrip(trip *models.Trip)
	RemoveTrip(tripId uuid.UUID)
	NewDriver(driverId uuid.UUID)
	RemoveDriver(driverId uuid.UUID)
	StartDispatching(ctx context.Context, period time.Duration, results chan models.DispatchResult)
}
