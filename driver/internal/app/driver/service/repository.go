package service

import (
	"context"
	"driver/internal/app/driver/api/codegen"
	"driver/internal/pkg/commands"
	models "driver/internal/pkg/models"

	"github.com/google/uuid"
)

type driversRepository interface {
	Get(ctx context.Context, id uuid.UUID) (*models.Driver, error)
	ChangeStatus(ctx context.Context, id uuid.UUID, isBusy bool, curTripId *uuid.UUID) error
}

type tripsRepository interface {
	Create(ctx context.Context, trip *models.Trip) error
	Get(ctx context.Context, id uuid.UUID) (*models.Trip, error)
	Remove(ctx context.Context, id uuid.UUID) error
	ChangeStatus(ctx context.Context, id uuid.UUID, status codegen.TripStatus) error
	SetDriverId(ctx context.Context, id uuid.UUID, driverId *uuid.UUID) error
}

type assignmentsRepository interface {
	Create(ctx context.Context, assignment *models.Assignment) error
	Get(ctx context.Context, tripId uuid.UUID) (*models.Assignment, error)
	AddCandidate(ctx context.Context, tripId uuid.UUID, candidate uuid.UUID) error
	AssignTo(ctx context.Context, tripId uuid.UUID, driverId uuid.UUID) error
	Stop(ctx context.Context, tripId uuid.UUID) error
}

type transactionManager interface {
	WithTransaction(ctx context.Context, f func() error) error
}

type eventsRepository interface {
	StartListeningToTripInboundEvents(ctx context.Context, callback func(ctx context.Context, event interface{})) error
	PushCommand(ctx context.Context, command commands.Command) error
	Close() error
}
