package service

import (
	"context"
	"driver/internal/app/driver/api/codegen"
	location "driver/internal/app/driver/api/codegen/clients"
	"driver/internal/app/driver/config"
	"driver/internal/app/driver/dispatch"
	"driver/internal/app/driver/polling"
	"driver/internal/app/driver/repository"
	"driver/internal/app/driver/tracer"
	"driver/internal/pkg/commands"
	"driver/internal/pkg/events"
	models "driver/internal/pkg/models"
	"reflect"
	"slices"

	"github.com/google/uuid"
	"github.com/juju/zaputil/zapctx"
)

type Service struct {
	repos  repositories
	trmngr transactionManager

	cfg          *config.Config
	dispatcher   dispatcher
	tripsPolling tripsPolling
}

type repositories struct {
	drivers     driversRepository
	trips       tripsRepository
	assignments assignmentsRepository
	events      eventsRepository
}

func NewService(ctx context.Context, cfg *config.Config) (*Service, error) {
	driversRepo, tripsRepo, assignmentsRepo, eventsRepo, client, err := repository.GetRepositories(ctx, cfg)
	if err != nil {
		return nil, err
	}

	trmngr, err := repository.NewTransactionManager(client)
	if err != nil {
		return nil, err
	}

	locationClient, err := location.NewClientWithResponses(cfg.ClientAddrs.Location)
	if err != nil {
		return nil, err
	}

	return &Service{
		repos: repositories{
			drivers:     driversRepo,
			trips:       tripsRepo,
			assignments: assignmentsRepo,
			events:      eventsRepo,
		},
		trmngr: trmngr,
		cfg:    cfg,
		// TODO: можно добавить здесь вычитку Trip'ов из базы,
		// чтобы при рестарте сервиса диспатч продолжался
		dispatcher:   dispatch.NewDispatcher(locationClient),
		tripsPolling: polling.NewTripsPolling(),
	}, nil
}

func (s *Service) Start(ctx context.Context) {
	go s.repos.events.StartListeningToTripInboundEvents(ctx, s.onEvent)

	results := make(chan models.DispatchResult) // TODO: make it buffered
	s.dispatcher.StartDispatching(ctx, s.cfg.Dispatch.Period, results)

	go s.startListeningToDispatchResults(ctx, results)
}

func (s *Service) Stop(ctx context.Context) {
	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	err := s.repos.events.Close()
	if err != nil {
		logger.Error(err.Error())
	}
}

func (s *Service) onEvent(ctx context.Context, event interface{}) {
	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	switch e := event.(type) {
	case events.TripCreatedEvent:
		s.onTripCreated(ctx, e)
	case events.TripCanceledEvent:
		s.onTripCanceled(ctx, e)
	default:
		logger.Sugar().Warnf("Unexpected kafka event with type: %s", reflect.TypeOf(e).String())
	}
}

func (s *Service) startListeningToDispatchResults(ctx context.Context, results chan models.DispatchResult) {
	logger := zapctx.Logger(ctx)
	defer logger.Sync()
	for result := range results {
		logger.Sugar().Infof("New dispatch result. trip_id=%s, driver_id=%s", result.Trip.Id, result.DriverId)
		pollingCh, err := s.tripsPolling.GetDriversChan(result.DriverId)
		if err != nil {
			logger.Sugar().Infof("Couldn't get polling chan for driver %s, will put trip %s to dispatch again",
				result.DriverId, result.Trip.Id)
			s.dispatcher.NewTrip(result.Trip)
			continue
		}
		err = s.repos.assignments.AddCandidate(ctx, result.Trip.Id, result.DriverId)
		if err != nil {
			logger.Sugar().Errorf("Couldn't add candidate to assignment for trip %s: %s", result.Trip.Id, err.Error())
			s.dispatcher.NewTrip(result.Trip)
			s.dispatcher.NewDriver(result.DriverId)
			continue
		}
		s.dispatcher.RemoveTrip(result.Trip.Id)
		s.dispatcher.RemoveDriver(result.DriverId)
		*pollingCh <- result.Trip
		close(*pollingCh)
	}
}

func (s *Service) onTripCreated(ctx context.Context, event events.TripCreatedEvent) {
	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	logger.Sugar().Infof("Start processing trip created event for trip_id=%s", event.TripId)

	trip := &models.Trip{
		Id:       event.TripId,
		From:     event.From,
		To:       event.To,
		Price:    event.Price,
		Status:   event.Status,
		DriverId: nil,
	}

	err := s.repos.trips.Create(ctx, trip)
	if err != nil {
		// TODO можно как-то поретраить
		logger.Sugar().Errorf("Error on trip creation: %s", err)
		return
	}
	err = s.repos.assignments.Create(ctx, &models.Assignment{
		TripId:       trip.Id,
		NeedToAssign: true,
		CandidateIds: make([]uuid.UUID, 0),
		AssignedToId: nil,
	})
	if err != nil {
		// TODO можно как-то поретраить
		logger.Sugar().Errorf("Error on creating assignment: %s", err)
		return
	}
	s.dispatcher.NewTrip(trip)
	logger.Sugar().Infof("Put trip %s to dispatch", event.TripId)
}

func (s *Service) onTripCanceled(ctx context.Context, event events.TripCanceledEvent) {
	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	logger.Sugar().Infof("Start processing trip canceled event for trip_id=%s", event.TripId)

	err := s.trmngr.WithTransaction(ctx, func() error {
		trip, err := s.repos.trips.Get(ctx, event.TripId)
		if err != nil {
			return err
		}

		if trip.Status == codegen.DRIVERSEARCH {
			err := s.repos.assignments.Stop(ctx, event.TripId)
			if err != nil && err != models.ErrAssignmentNotFound {
				// TODO: log on "assignment not found" here
				return err
			}
		}

		err = s.repos.trips.ChangeStatus(ctx, event.TripId, codegen.CANCELED)
		if err != nil {
			return err
		}

		if trip.DriverId != nil {
			err := s.repos.drivers.ChangeStatus(ctx, *trip.DriverId, false, nil)
			if err != nil && err != models.ErrDriverNotFound {
				// TODO: log on "driver not found" here
				return err
			}
		}
		return nil
	})

	if err != nil {
		logger.Sugar().Errorf("Error on handling trip canceled event for trip %s: %s", event.TripId, err.Error())
	}
}

func (s *Service) GetTrips(ctx context.Context, driverId uuid.UUID) ([]models.Trip, error) {
	ctx, span := tracer.Tracer.Start(ctx, "GetTrips")
	defer span.End()

	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	driver, err := s.repos.drivers.Get(ctx, driverId)
	if err == models.ErrDriverNotFound {
		return nil, err
	}
	if err != nil {
		logger.Sugar().Errorf("Error on getting driver %s: %s", driverId, err)
		return nil, err
	}
	if driver.CurTripId != nil {
		return nil, models.ErrDriverOnTrip
	}

	s.dispatcher.NewDriver(driverId)
	defer s.dispatcher.RemoveDriver(driverId)

	resultCh := make(chan *models.Trip)
	s.tripsPolling.AddDriver(driverId, &resultCh)
	defer s.tripsPolling.RemoveDriver(driverId)

	result := make([]models.Trip, 0)
	for trip := range resultCh {
		result = append(result, *trip)
	}

	return result, nil
}

func (s *Service) AcceptTrip(ctx context.Context, tripId uuid.UUID, driverId uuid.UUID) error {
	ctx, span := tracer.Tracer.Start(ctx, "AcceptTrip")
	defer span.End()

	logger := zapctx.Logger(ctx)
	defer logger.Sync()

	driver, err := s.repos.drivers.Get(ctx, driverId)
	if err == models.ErrDriverNotFound {
		return err
	}
	if err != nil {
		logger.Sugar().Errorf("Error on getting driver %s: %s", driverId, err)
		return err
	}

	trip, err := s.repos.trips.Get(ctx, tripId)
	if err == models.ErrTripNotFound {
		return err
	}
	if err != nil {
		logger.Sugar().Errorf("Error on getting trip %s: %s", tripId, err)
		return err
	}

	if driver.CurTripId != nil && *driver.CurTripId == tripId && trip.Status == codegen.DRIVERFOUND {
		// Idempotency
		return nil
	}
	if driver.CurTripId != nil {
		return models.ErrDriverOnTrip
	}

	assignment, err := s.repos.assignments.Get(ctx, tripId)
	if err == models.ErrAssignmentNotFound {
		logger.Sugar().Infof("Didn't find assignment for trip %s", tripId)
		return models.ErrTripNotFound
	}

	isCandidate := (slices.Index(assignment.CandidateIds, driverId) != -1)
	if !isCandidate || !assignment.NeedToAssign {
		return models.ErrCantAccept
	}

	err = s.trmngr.WithTransaction(ctx, func() error {
		if err := s.repos.drivers.ChangeStatus(ctx, driverId, true, &tripId); err != nil {
			return err
		}
		if err := s.repos.trips.ChangeStatus(ctx, tripId, codegen.DRIVERFOUND); err != nil {
			return err
		}
		if err := s.repos.trips.SetDriverId(ctx, tripId, &driverId); err != nil {
			return err
		}
		if err := s.repos.assignments.AssignTo(ctx, tripId, driverId); err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		logger.Sugar().Errorf("Error on assignment transaction, trip_id=%s, driver_id=%s, err=%s", tripId, driverId, err)
		return err
	}

	err = s.repos.events.PushCommand(ctx, commands.NewAcceptTripCommand(tripId, driverId))
	if err != nil {
		logger.Sugar().Errorf("Error on pushing command: %s", err)
	}
	return err
}

func (s *Service) CancelTripByDriver(ctx context.Context, tripId uuid.UUID, driverId uuid.UUID, reason *string) error {
	ctx, span := tracer.Tracer.Start(ctx, "CancelTripByDriver")
	defer span.End()

	err := s.trmngr.WithTransaction(ctx, func() error {
		_, err := s.repos.drivers.Get(ctx, driverId)
		if err != nil {
			return models.ErrDriverNotFound
		}

		trip, err := s.repos.trips.Get(ctx, tripId)
		if err != nil {
			return err
		}
		if trip.DriverId == nil || *trip.DriverId != driverId {
			return models.ErrTripNotFound
		}
		if trip.Status == codegen.CANCELED {
			// Idempotency
			return nil
		}
		if !trip.IsCancelableByDriver() {
			return models.ErrCantCancel
		}

		err = s.repos.trips.ChangeStatus(ctx, tripId, codegen.CANCELED)
		if err != nil {
			return err
		}

		return s.repos.drivers.ChangeStatus(ctx, driverId, false, nil)
	})
	if err != nil {
		return err
	}

	// вообще тут нужно держать транзакцию, т.к. может что-то зафейлится
	// в момент пуша в Кафку
	return s.repos.events.PushCommand(ctx, commands.NewCancelTripCommand(tripId, reason))
}

func (s *Service) EndTrip(ctx context.Context, tripId uuid.UUID, driverId uuid.UUID) error {
	ctx, span := tracer.Tracer.Start(ctx, "EndTrip")
	defer span.End()

	err := s.trmngr.WithTransaction(ctx, func() error {
		_, err := s.repos.drivers.Get(ctx, driverId)
		if err != nil {
			return models.ErrDriverNotFound
		}

		trip, err := s.repos.trips.Get(ctx, tripId)
		if err != nil {
			return err
		}
		if trip.DriverId == nil || *trip.DriverId != driverId {
			return models.ErrTripNotFound
		}
		if trip.Status == codegen.ENDED {
			// Idempotency
			// А вообще наверное на уровне слоя api должны решать,
			// что отдавать в таких случаях
			return nil
		}
		if !trip.CanBeEndedByDriver() {
			return models.ErrCantEnd
		}

		err = s.repos.trips.ChangeStatus(ctx, tripId, codegen.ENDED)
		if err != nil {
			return err
		}

		return s.repos.drivers.ChangeStatus(ctx, driverId, false, nil)
	})
	if err != nil {
		return err
	}

	// вообще тут нужно держать транзакцию, т.к. может что-то зафейлится
	// в момент пуша в Кафку
	return s.repos.events.PushCommand(ctx, commands.NewEndTripCommand(tripId))
}

func (s *Service) StartTrip(ctx context.Context, tripId uuid.UUID, driverId uuid.UUID) error {
	ctx, span := tracer.Tracer.Start(ctx, "StartTrip")
	defer span.End()

	err := s.trmngr.WithTransaction(ctx, func() error {
		_, err := s.repos.drivers.Get(ctx, driverId)
		if err != nil {
			return models.ErrDriverNotFound
		}

		trip, err := s.repos.trips.Get(ctx, tripId)
		if err != nil {
			return err
		}
		if trip.DriverId == nil || *trip.DriverId != driverId {
			return models.ErrTripNotFound
		}
		if trip.Status == codegen.STARTED {
			// Idempotency
			// А вообще наверное на уровне слоя api должны решать,
			// что отдавать в таких случаях
			return nil
		}
		if !trip.CanBeStarted() {
			return models.ErrCantStart
		}

		return s.repos.trips.ChangeStatus(ctx, tripId, codegen.STARTED)
	})
	if err != nil {
		return err
	}

	// вообще тут нужно держать транзакцию, т.к. может что-то зафейлится
	// в момент пуша в Кафку
	return s.repos.events.PushCommand(ctx, commands.NewStartTripCommand(tripId))
}

func (s *Service) GetCurrentTripInfo(ctx context.Context, tripId uuid.UUID, driverId uuid.UUID) (*models.Trip, error) {
	ctx, span := tracer.Tracer.Start(ctx, "GetCurrentTripInfo")
	defer span.End()

	driver, err := s.repos.drivers.Get(ctx, driverId)
	if err != nil {
		return nil, err
	}

	if driver.CurTripId == nil || *driver.CurTripId != tripId {
		return nil, models.ErrTripNotFound
	}

	return s.repos.trips.Get(ctx, tripId)
}
