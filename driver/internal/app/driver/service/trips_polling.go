package service

import (
	models "driver/internal/pkg/models"

	"github.com/google/uuid"
)

type tripsPolling interface {
	AddDriver(driverId uuid.UUID, result *chan *models.Trip)
	GetDriversChan(driverId uuid.UUID) (*chan *models.Trip, error)
	RemoveDriver(driverId uuid.UUID)
}
