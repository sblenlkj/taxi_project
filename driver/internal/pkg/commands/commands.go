package commands

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type commonCommandData struct {
	Id              uuid.UUID `json:"id"`
	Source          string    `json:"source"`
	Type            string    `json:"type"`
	DataContentType string    `json:"datacontenttype"`
	Time            string    `json:"time"`
}

func newCommonCommandData(commandType string) commonCommandData {
	return commonCommandData{
		Id:              uuid.New(),
		Source:          "/driver",
		Type:            commandType,
		DataContentType: "application/json",
		Time:            time.Now().UTC().Format(time.RFC3339),
	}
}

type acceptTripCommandData struct {
	TripId   uuid.UUID `json:"trip_id"`
	DriverId uuid.UUID `json:"driver_id"`
}

type AcceptTripCommand struct {
	commonCommandData
	Data acceptTripCommandData `json:"data"`
}

func NewAcceptTripCommand(tripId uuid.UUID, driverId uuid.UUID) *AcceptTripCommand {
	return &AcceptTripCommand{
		commonCommandData: newCommonCommandData("trip.command.accept"),
		Data: acceptTripCommandData{
			TripId:   tripId,
			DriverId: driverId,
		},
	}
}

func (c *AcceptTripCommand) Key() []byte {
	return c.Data.TripId[:]
}

func (c *AcceptTripCommand) MarshalJson() ([]byte, error) {
	return json.Marshal(c)
}

type startTripCommandData struct {
	TripId uuid.UUID `json:"trip_id"`
}

type StartTripCommand struct {
	commonCommandData
	Data startTripCommandData `json:"data"`
}

func NewStartTripCommand(tripId uuid.UUID) *StartTripCommand {
	return &StartTripCommand{
		commonCommandData: newCommonCommandData("trip.command.start"),
		Data: startTripCommandData{
			TripId: tripId,
		},
	}
}

func (c *StartTripCommand) Key() []byte {
	return c.Data.TripId[:]
}

func (c *StartTripCommand) MarshalJson() ([]byte, error) {
	return json.Marshal(c)
}

type cancelTripCommandData struct {
	TripId uuid.UUID `json:"trip_id"`
	Reason *string   `json:"reason,omitempty"`
}

type CancelTripCommand struct {
	commonCommandData
	Data cancelTripCommandData `json:"data"`
}

func NewCancelTripCommand(tripId uuid.UUID, reason *string) *CancelTripCommand {
	return &CancelTripCommand{
		commonCommandData: newCommonCommandData("trip.command.cancel"),
		Data: cancelTripCommandData{
			TripId: tripId,
			Reason: reason,
		},
	}
}

func (c *CancelTripCommand) Key() []byte {
	return c.Data.TripId[:]
}

func (c *CancelTripCommand) MarshalJson() ([]byte, error) {
	return json.Marshal(c)
}

type endTripCommandData struct {
	TripId uuid.UUID `json:"trip_id"`
}

type EndTripCommand struct {
	commonCommandData
	Data endTripCommandData `json:"data"`
}

func NewEndTripCommand(tripId uuid.UUID) *EndTripCommand {
	return &EndTripCommand{
		commonCommandData: newCommonCommandData("trip.command.end"),
		Data: endTripCommandData{
			TripId: tripId,
		},
	}
}

func (c *EndTripCommand) Key() []byte {
	return c.Data.TripId[:]
}

func (c *EndTripCommand) MarshalJson() ([]byte, error) {
	return json.Marshal(c)
}

type Command interface {
	MarshalJson() ([]byte, error)
	Key() []byte
}
