package events

import (
	"driver/internal/app/driver/api/codegen"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

var ErrUnknownEvent = errors.New("unknown event")

type TripCreatedEvent struct {
	TripId uuid.UUID          `json:"trip_id"`
	Price  codegen.Money      `json:"price"`
	Status codegen.TripStatus `json:"status"`
	From   codegen.Position   `json:"from"`
	To     codegen.Position   `json:"to"`
}

type TripCanceledEvent struct {
	TripId uuid.UUID `json:"trip_id"`
}

const (
	TRIP_CREATED  eventType = "trip.event.created"
	TRIP_ACCEPTED eventType = "trip.event.accepted"
	TRIP_CANCELED eventType = "trip.event.canceled"
	TRIP_STARTED  eventType = "trip.event.started"
	TRIP_ENDED    eventType = "trip.event.ended"
)

type eventType = string

type eventUnmarshallingHelper struct {
	Type eventType        `json:"type"`
	Data *json.RawMessage `json:"data"`
}

func ParseEvent(rawEvent []byte) (interface{}, error) {
	var helper eventUnmarshallingHelper
	err := json.Unmarshal(rawEvent, &helper)
	if err != nil {
		return nil, err
	}

	switch helper.Type {
	case TRIP_CREATED:
		var event TripCreatedEvent
		err = json.Unmarshal(*helper.Data, &event)
		if err != nil {
			return nil, err
		}
		return event, nil
	case TRIP_CANCELED:
		var event TripCanceledEvent
		err = json.Unmarshal(*helper.Data, &event)
		if err != nil {
			return nil, err
		}
		return event, nil
	default:
		return nil, ErrUnknownEvent
	}
}
