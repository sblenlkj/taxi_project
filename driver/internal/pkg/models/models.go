package models

import (
	"driver/internal/app/driver/api/codegen"
	"errors"

	"github.com/google/uuid"
)

type Driver struct {
	UserId    uuid.UUID  `bson:"user_id"`
	IsBusy    bool       `bson:"is_busy"`
	CurTripId *uuid.UUID `bson:"cur_trip_id,omitempty"`
}

var ErrDriverNotFound = errors.New("driver not found")
var ErrDriverOnTrip = errors.New("driver on trip")

type Assignment struct {
	TripId       uuid.UUID   `bson:"trip_id"`
	NeedToAssign bool        `bson:"need_to_assign"`
	CandidateIds []uuid.UUID `bson:"candidate_ids"`
	AssignedToId *uuid.UUID  `bson:"assigned_to_id,omitempty"`
}

var ErrAssignmentNotFound = errors.New("assignment not found")

type Trip struct {
	Id       uuid.UUID          `bson:"id"`
	From     codegen.Position   `bson:"from"`
	To       codegen.Position   `bson:"to"`
	Price    codegen.Money      `bson:"price"`
	Status   codegen.TripStatus `bson:"status"`
	DriverId *uuid.UUID         `bson:"driver_id,omitempty"`
}

func (t *Trip) IsCancelableByDriver() bool {
	switch t.Status {
	case codegen.DRIVERFOUND, codegen.ONPOSITION, codegen.STARTED:
		return true
	default:
		return false
	}
}

func (t *Trip) CanBeEndedByDriver() bool {
	return t.Status == codegen.STARTED
}

func (t *Trip) CanBeStarted() bool {
	return t.Status == codegen.DRIVERFOUND
}

var ErrTripNotFound = errors.New("trip not found")

var ErrCantAccept = errors.New("this driver can't accept the trip")
var ErrCantStart = errors.New("can't start trip in current status")
var ErrCantCancel = errors.New("can't cancel trip in current status")
var ErrCantEnd = errors.New("can't end trip in current status")

type DispatchResult struct {
	Trip     *Trip
	DriverId uuid.UUID
}
