FROM alpine

WORKDIR /app

COPY --from=location_service_build:develop /app/cmd/main ./app

CMD ["/app/app"]
