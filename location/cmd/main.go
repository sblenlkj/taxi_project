package main

import (
	"log"
	location_app "location/internal/app"
)

func main() {
	config, err := location_app.NewConfigEnv()
	if err != nil {
		log.Fatal(err)
	}

	a, err := location_app.New(config)
	if err != nil {
		log.Fatal(err)
	}

	if err = a.Serve(); err != nil {
		log.Fatal(err)
	}
}
