package app

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pressly/goose/v3"

	"location/internal/http"
	"location/internal/logger"
	"location/internal/otel"
	"location/internal/repo/repopostgress"
	"location/internal/service/locationsvc"

	"location/internal/tracer"

	"github.com/juju/zaputil/zapctx"
	"go.uber.org/zap"
)

type App struct {
	config     *Config
	db         *sqlx.DB
	httpServer http.Server

	Logger                   *zap.Logger
	otelProviderShutdownFunc func()
}

func New(config *Config) (*App, error) {
	logger, err := logger.GetLogger(config.App.Debug)
	logger = logger.With(zap.Any("Struct", "app"))

	logger.Info("Application creation started")

	if err != nil {
		logger.Error("GetLogger error")
		return nil, err
	}

	ctx := zapctx.WithLogger(context.Background(), logger)
	db, err := initDB(ctx, &config.Database)
	if err != nil {
		return nil, err
	}

	location_repo := repopostgress.New(db)
	logger.Info("Repo is initialised")

	location_Service := locationsvc.New(location_repo)
	logger.Info("Service is created")

	srv := http.New(&config.HTTP, location_Service, config.App.Debug)
	logger.Info("Server is created")

	a := &App{
		config:     config,
		db:         db,
		httpServer: srv,
		Logger:     logger,
	}

	a.Logger.Info("Application is created")

	a.Logger.Info("Init otel provider")
	a.otelProviderShutdownFunc = otel.InitProvider()

	return a, nil
}

func initDB(ctx context.Context, config *DatabaseConfig) (*sqlx.DB, error) {
	logger := zapctx.Logger(ctx)
	logger = logger.With(zap.Any("Function", "initDB"))

	db, err := sqlx.Open(config.Driver_name, config.DSN)
	if err != nil {
		logger.Error("sqlx Open Error")
		return nil, fmt.Errorf("unable to connect to database: %w", err)
	}

	db.DB.SetMaxOpenConns(100)
	db.DB.SetMaxIdleConns(10)
	db.DB.SetConnMaxLifetime(0)

	if err = db.PingContext(ctx); err != nil {
		logger.Error("PingContext Error")
		return nil, err
	}

	// migrations
	logger.Info("Migrations...")

	fs := os.DirFS(config.MigrationsDir)
	goose.SetBaseFS(fs)

	if err = goose.SetDialect(config.Driver_name); err != nil {
		panic(err)
	}

	if err = goose.UpContext(ctx, db.DB, "."); err != nil {
		panic(err)
	}

	logger.Info("Migrations Done")
	return db, nil
}

func (a *App) Serve() error {
	serviceName := "location"
	serviceVersion := "1.0.0"

	otelShutdown, err := tracer.SetupOTelSDK(context.Background(), serviceName, serviceVersion)
	if err != nil {
		return err
	}
	// Handle shutdown properly so nothing leaks.
	defer func() {
		err = errors.Join(err, otelShutdown(context.Background()))
	}()

	done := make(chan os.Signal, 1)

	signal.Notify(done, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		if err := a.httpServer.Serve(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatal(err.Error())
		}
	}()

	<-done

	a.Shutdown()

	return nil
}

func (a *App) Shutdown() {
	a.Logger.Info("Shuting down...")

	ctx, cancel := context.WithTimeout(context.Background(), a.config.App.ShutdownTimeout)
	defer cancel()

	a.otelProviderShutdownFunc()

	a.httpServer.Shutdown(ctx)

	a.Logger.Info("The End")
}
