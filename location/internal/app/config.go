package app

import (
	"os"
	"strconv"
	"time"

	"location/internal/http"
)

type AppConfig struct {
	Debug           bool
	ShutdownTimeout time.Duration
}

type DatabaseConfig struct {
	DSN           string
	MigrationsDir string
	Driver_name   string
}

type Config struct {
	App      AppConfig
	HTTP     http.Config
	Database DatabaseConfig
}

func getEnv(key string, default_value string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return default_value
}

func NewConfigEnv() (*Config, error) {
	ShutdownTimeout, err := strconv.Atoi(getEnv("LOCATION_SHUTDOWN_TIMEOUT", "10"))
	if err != nil {
		return nil, err
	}

	Debug, err := strconv.ParseBool(getEnv("LOCATION_DEBUG", "true"))
	if err != nil {
		return nil, err
	}

	cnf := Config{
		App: AppConfig{
			Debug:           Debug,
			ShutdownTimeout: time.Duration(ShutdownTimeout) * time.Second,
		},
		Database: DatabaseConfig{
			DSN:           getEnv("LOCATION_DNS", "postgres://postgres:postgres@location_db:5432/example?sslmode=disable"),
			MigrationsDir: getEnv("LOCATION_MIGRATION_DIR", "migrations"),
			Driver_name:   getEnv("LOCATION_DRIVER_NAME", "postgres"),
		},
		HTTP: http.Config{
			BasePath:     getEnv("LOCATION_BASE_PATH", "/"),
			ServeAddress: getEnv("LOCATION_SERVER_ADDRESS", ":80"),
		},
	}

	return &cnf, nil
}
