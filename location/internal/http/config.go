package http

type Config struct {
	BasePath     string
	ServeAddress string
}
