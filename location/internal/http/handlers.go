package http

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"

	"github.com/go-chi/chi"

	"location/internal/model"
	"location/internal/service"
	"location/internal/tracer"

	"github.com/juju/zaputil/zapctx"
)

func getParamsFromQuery(v url.Values) (float64, *model.LatLngLiteral) {
	latLng := &model.LatLngLiteral{}

	rad, _ := strconv.ParseFloat(v.Get("radius"), 64)
	latLng.Lat, _ = strconv.ParseFloat(v.Get("lat"), 64)
	latLng.Lng, _ = strconv.ParseFloat(v.Get("lng"), 64)

	return rad, latLng
}

func (s *MyServer) getDrivers(w http.ResponseWriter, r *http.Request) {
	ctx, span := tracer.Tracer.Start(r.Context(), "Server getDrivers")
	defer span.End()

	s.counter_ask_for_drivers.Inc()

	s.logger.Info("Geting drivers...")

	rad, latLng := getParamsFromQuery(r.URL.Query())

	s.histogram_radius.Observe(rad)

	ctx = zapctx.WithLogger(ctx, s.logger)
	drivers, err := s.locationService.GetAllDrivers(ctx, rad, latLng)

	if err != nil {
		s.histogram_drivers_available.Observe(0)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	s.logger.Info("Done")

	s.histogram_drivers_available.Observe(float64(len(drivers)))

	writeJSONResponse(w, http.StatusOK, drivers)
}

func (s *MyServer) updateDriverLocation(w http.ResponseWriter, r *http.Request) {
	ctx, span := tracer.Tracer.Start(r.Context(), "Server updateDriverLocation")
	defer span.End()

	s.counter_update.Inc()

	s.logger.Info("Updating driver information...")

	latLng := &model.LatLngLiteral{}

	if err := json.NewDecoder(r.Body).Decode(latLng); err != nil {
		writeError(w, http.StatusInternalServerError, err)
		return
	}

	id := chi.URLParam(r, "driver_id")

	ctx = zapctx.WithLogger(ctx, s.logger)
	err := s.locationService.ChangeOneDriverData(ctx, id, latLng) //
	if err != nil {
		if errors.Is(service.ErrIncorectLocation, err) {
			s.gauge_coordinates_correct.Dec()
		} else {
			s.gauge_coordinates_correct.Inc()
		}

		w.WriteHeader(http.StatusBadRequest)
		return
	}

	s.gauge_coordinates_correct.Inc()

	s.logger.Info("Done")

	writeJSONResponse(w, http.StatusOK, "Success operation")
}
