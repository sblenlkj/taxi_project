package http

import (
	"context"
	"net/http"
	"github.com/go-chi/chi"
	"location/internal/service/locationsvc"

	"location/internal/logger"
	"go.uber.org/zap"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var _ Server = &MyServer{}

type MyServer struct {
	config                      *Config
	locationService 		    *locationsvc.LocationService
	server 					    *http.Server
	logger 					    *zap.Logger
	version 				    string

	counter_ask_for_drivers     prometheus.Counter
	counter_update 		        prometheus.Counter
	gaugevec_info    		    *prometheus.GaugeVec
	gauge_coordinates_correct   prometheus.Gauge
	histogram_drivers_available prometheus.Histogram
	histogram_radius            prometheus.Histogram
}

func (s *MyServer) NewRouter() *chi.Mux {
	r := chi.NewRouter()

	apiRouter := chi.NewRouter()
	apiRouter.Get("/drivers", s.getDrivers)
	apiRouter.Post("/drivers/{driver_id}/location", s.updateDriverLocation)

	r.Mount(s.config.BasePath, apiRouter)

	return r
}
func (s *MyServer) Serve() error {
	s.gaugevec_info.With(prometheus.Labels{"version": s.version}).Set(1)
	
	r := s.NewRouter()
	s.server = &http.Server{Addr: s.config.ServeAddress, Handler: r}

	s.logger.Info("Server is started")

	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(":9000", nil)

	return s.server.ListenAndServe()
}

func (s *MyServer) Shutdown(ctx context.Context) {
	_ = s.server.Shutdown(ctx)
}

func New(config *Config, locationService *locationsvc.LocationService, debug bool) *MyServer {
	logger, err := logger.GetLogger(debug)
	if err != nil {
		logger.Error("GetLogger error")
		return nil
	}

	logger = logger.With(zap.Any("Struct", "adapter"))

	counter_ask_for_drivers := promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "location", Name: "ask_for_drivers_counter", Help: "Get drivers request counter",
	})

	counter_update := promauto.NewCounter(prometheus.CounterOpts{
		Namespace: "location", Name: "update_counter", Help: "Update request counter",
	})

	gauge_coordinates_correct := promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: "location", Name: "coordinates", Help: "Coordinates request correct / wrong",
	})

	gaugevec_info := promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "location", Name: "info", Help: "Information about adapter - version",
	}, []string{"version"})

	version := "1.1.1"

	histogram_drivers_available := promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: "location",
		Name: "histogram_drivers_available",
		Buckets: prometheus.LinearBuckets(0, 2, 5),
		Help: "Drivers who are near to the client",
	})

	histogram_radius := promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: "location",
		Name: "histogram_radius",
		Buckets: prometheus.LinearBuckets(0, 3, 10),
		Help: "radius histogram",
	})



	return &MyServer{
		logger: logger,
		config:      config,
		locationService: locationService,
		version: version,

		counter_ask_for_drivers: counter_ask_for_drivers,
		counter_update: counter_update,
		gaugevec_info: gaugevec_info,
		histogram_radius: histogram_radius,
		histogram_drivers_available: histogram_drivers_available,
		gauge_coordinates_correct: gauge_coordinates_correct,
	}
}