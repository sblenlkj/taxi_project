package http_test

import (
	"testing"
	"net/http/httptest"
	"net/http"
	"fmt"
	"bytes"
	"io"
	"encoding/json"

	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"

	my_http "location/internal/http"
	"location/internal/model"
	"location/internal/repo/mock"
	"location/internal/service/locationsvc"
)


func twoDrivers() []model.Driver {
	return []model.Driver{{
		ID: "ID_1",
	}, {
		ID: "ID_2",
	}}
}

func requireBodyMatchAccount(t *testing.T, body *bytes.Buffer, drivers []model.Driver) {
	data, err := io.ReadAll(body)
	require.NoError(t, err)

	var gotDrivers []model.Driver
	err = json.Unmarshal(data, &gotDrivers )

	require.NoError(t, err)
	require.Equal(t, drivers, gotDrivers)
}

func TestGetDriversAPI(t *testing.T) {
	drivers := twoDrivers()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockedRepo(ctrl)

	repo.EXPECT().
		GetAllDrivers(gomock.Any(), gomock.Any(), gomock.Any()).
		Times(1).
		Return(drivers, nil)

	serviceMy := locationsvc.New(repo)

	config := my_http.Config{BasePath: "/", ServeAddress: "80"}
	serverMy := my_http.New(&config, serviceMy, true)
	recorder := httptest.NewRecorder()

	url := fmt.Sprintf("/drivers?radius=%f&lat=%f&lng=%f", 10.0, 10.0, 10.0)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	require.NoError(t, err)

	r := serverMy.NewRouter()
	r.ServeHTTP(recorder, request)
	require.Equal(t, http.StatusOK, recorder.Code)

	requireBodyMatchAccount(t, recorder.Body, drivers)
}

func TestChangeOneDriverCoordinatesNotExist(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockedRepo(ctrl)
	serviceMy := locationsvc.New(repo)

	config := my_http.Config{BasePath: "/", ServeAddress: "80"}
	serverMy := my_http.New(&config, serviceMy, true)
	recorder := httptest.NewRecorder()

	url := fmt.Sprintf("/drivers/%s/location", "12")
	latLng_not_exist := &model.LatLngLiteral{Lat: 100, Lng: 100}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(latLng_not_exist)
	require.NoError(t, err)

	request, err := http.NewRequest(http.MethodPost, url, &buf)
	require.NoError(t, err)

	r := serverMy.NewRouter()
	r.ServeHTTP(recorder, request)
	require.Equal(t, http.StatusBadRequest, recorder.Code)
}

func TestChangeOneDriverExist(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockedRepo(ctrl)
	serviceMy := locationsvc.New(repo)

	repo.EXPECT().
		WithNewTx(gomock.Any(), gomock.Any(), gomock.Any()).
		Times(1).
		Return(nil)

	config := my_http.Config{BasePath: "/", ServeAddress: "80"}
	serverMy := my_http.New(&config, serviceMy, true)
	recorder := httptest.NewRecorder()

	url := fmt.Sprintf("/drivers/%s/location", "12")
	latLng_exist := &model.LatLngLiteral{Lat: 50, Lng: 100}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(latLng_exist)
	require.NoError(t, err)

	request, err := http.NewRequest(http.MethodPost, url, &buf)
	require.NoError(t, err)

	r := serverMy.NewRouter()
	r.ServeHTTP(recorder, request)
	require.Equal(t, http.StatusOK, recorder.Code)
}
