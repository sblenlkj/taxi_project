package model

type LatLngLiteral struct {
	Lat float64 `json:"lat" db:"lat"`
	Lng float64 `json:"lng"  db:"lng"`
}
