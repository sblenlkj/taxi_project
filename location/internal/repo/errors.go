package repo

import "errors"

var (
	ErrDriverIDNotExist = errors.New("driver with the same index does not exist - Driver not found")
	ErrDriversNotFound = errors.New("there is no driver in the area - Drivers not found")
)
