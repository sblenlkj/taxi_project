package repo

import (
	"context"
	"database/sql"
	"location/internal/model"
)

//go:generate go run github.com/golang/mock/mockgen -mock_names Repo=MockedRepo -package mock -destination=./mock/repo_mock.go -source=./repo_interface.go

type Repo interface {
	WithNewTx(ctx context.Context, opts *sql.TxOptions, f func(ctx context.Context) error) error
	GetAllDrivers(ctx context.Context, rad float64, latLng *model.LatLngLiteral) ([]model.Driver, error)
	ChangeOneDriverData(ctx context.Context, id string, latLng *model.LatLngLiteral) error
}
