package repopostgress

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"

	sq "github.com/Masterminds/squirrel"

	"location/internal/model"
	"location/internal/repo"
	"location/internal/tracer"

	"github.com/juju/zaputil/zapctx"
	"go.uber.org/zap"
)

var _ repo.Repo = &LocationRepo{}

type LocationRepo struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *LocationRepo {
	return &LocationRepo{db: db}
}

func (r *LocationRepo) GetAllDrivers(ctx context.Context, rad float64, latLng *model.LatLngLiteral) ([]model.Driver, error) {
	_, span := tracer.Tracer.Start(ctx, "Repo - GetAllDrivers")
	defer span.End()

	logger := zapctx.Logger(ctx)
	logger = logger.With(zap.Any("Repo", "GetAllDrivers"))
	logger.Info("GetAllDrivers started")

	query := "SELECT driver_id FROM locations"
	query = fmt.Sprintf("%s WHERE acos(sin(radians($1)) * sin(radians(lat)) + cos(radians($1)) * cos(radians(lat)) * cos(radians($2) - radians(lng))) * 6371 <= $3;", query)

	rad2 := rad * rad
	logger.Sugar().Infof("GetAllDrivers args: lat - %f, lng - %f, rad2 - %f",latLng.Lat, latLng.Lng, rad2)

	args := []interface{}{latLng.Lat, latLng.Lng, rad2}
	rows, err := r.db.QueryxContext(ctx, query, args...)

	if err != nil {
		logger.Error("QueryxContext error")
		return nil, err
	}

	defer rows.Close()

	drivers := make([]model.Driver, 0)

	for rows.Next() {
		d := model.Driver{}

		if err = rows.StructScan(&d); err != nil {
			logger.Error("Rows Next error")
			return nil, err
		}

		drivers = append(drivers, d)
	}

	if len(drivers) == 0 {
		logger.Info("GetAllDrivers ended: no driver found")
		return nil, repo.ErrDriversNotFound
	}
	
	logger.Sugar().Infof("GetAllDrivers ended: %s drivers found", len(drivers))

	return drivers, nil
}

func (r *LocationRepo) ChangeOneDriverData(ctx context.Context, id string, latLng *model.LatLngLiteral) (error) {
	_, span := tracer.Tracer.Start(ctx, "Repo - ChangeOneDriverData")
	defer span.End()
	
	logger := zapctx.Logger(ctx)
	logger = logger.With(zap.Any("Repo", "ChangeOneDriverData"))
	logger.Info("ChangeOneDriverData started")
	
	sql_script, args, err := sq.
		Update("locations").
		Set("lat", latLng.Lat).
		Set("lng", latLng.Lng).
		Where("driver_id = ?", id).
		Suffix("RETURNING lat, lng").
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		logger.Error("sql creation error")
		return err
	}

	var new_lat, new_lng float64
	err = r.db.QueryRowContext(ctx, sql_script, args...).Scan(&new_lat, &new_lng)
	if err == sql.ErrNoRows {
		logger.Error("Sqan error - the driver with the same index does not exist")
		return repo.ErrDriverIDNotExist
	}
	
	if err != nil {
		logger.Error("Sqan error")
		return err
	}

	logger.Sugar().Infof("ChangeOneDriverData ended: driver with id = %s is in %f %f now", id, new_lat, new_lng)
	return nil 
}

func (r *LocationRepo) WithNewTx(ctx context.Context, opts *sql.TxOptions, f func(ctx context.Context) error) error {
	ctx, span := tracer.Tracer.Start(ctx, "Repo - WithNewTx")
	defer span.End()

	tx, err := r.db.BeginTx(ctx, opts)
	if err != nil {
		return err
	}

	defer func() {
		rollbackErr := tx.Rollback()
		if rollbackErr != nil && !errors.Is(rollbackErr, sql.ErrTxDone) {
			err = rollbackErr
		}
	}()

	fErr := f(ctx)
	if fErr != nil {
		_ = tx.Rollback()
		return fErr
	}

	return tx.Commit()
}
