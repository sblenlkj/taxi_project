package service

import "errors"

var (
	ErrIncorectLocation = errors.New("cordinates do not exist - Incorrect location")
)

