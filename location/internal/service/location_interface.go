package service

import (
	"context"
	"location/internal/model"
)

type Location interface {
	GetAllDrivers(ctx context.Context, rad float64, latLng *model.LatLngLiteral) ([]model.Driver, error)
	ChangeOneDriverData(ctx context.Context, id string, latLng *model.LatLngLiteral) (error)
}
