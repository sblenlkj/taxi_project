package locationsvc

import (
	"context"
	"database/sql"

	"location/internal/model"
	"location/internal/repo"
	"location/internal/service"
	"location/internal/tracer"

	"github.com/juju/zaputil/zapctx"
	"go.uber.org/zap"
)

var _ service.Location = &LocationService{}

type LocationService struct {
	rep repo.Repo
}

func New(rep repo.Repo) *LocationService {
	return &LocationService{rep: rep}
}

func (l *LocationService) GetAllDrivers(ctx context.Context, rad float64, latLng *model.LatLngLiteral) ([]model.Driver, error) {
	ctx, span := tracer.Tracer.Start(ctx, "GetAllDrivers")
	defer span.End()

	logger := zapctx.Logger(ctx)
	logger = logger.With(zap.Any("Service", "Get all drivers"))
	logger.Info("GetAllDrivers from service")
	ctx = zapctx.WithLogger(ctx, logger)

	return l.rep.GetAllDrivers(ctx, rad, latLng)
}

func (l *LocationService) ChangeOneDriverData(ctx context.Context, id string, latLng *model.LatLngLiteral) error {
	ctx, span := tracer.Tracer.Start(ctx, "ChangeOneDriverData")
	defer span.End()

	logger := zapctx.Logger(ctx)
	logger = logger.With(zap.Any("Service", "Change one driver data"))
	logger.Info("ChangeOneDriverData from service")
	ctx = zapctx.WithLogger(ctx, logger)

	if latLng.Lat > 90 || latLng.Lat < -90 || latLng.Lng > 180 || latLng.Lng < -180 {
		logger.Error("Coordinates do not exist")
		return service.ErrIncorectLocation
	}

	return l.rep.WithNewTx(ctx, &sql.TxOptions{Isolation: sql.LevelDefault}, func(ctx context.Context) error {
		return l.rep.ChangeOneDriverData(ctx, id, latLng)
	})
}
