package locationsvc_test

import (
	"context"
	"testing"

	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"

	"location/internal/model"
	"location/internal/repo/mock"
	"location/internal/service"
	"location/internal/service/locationsvc"
)

func twoDrivers() []model.Driver {
	return []model.Driver{{
		ID: "ID_1",
	}, {
		ID: "ID_2",
	}}
}

func TestGetDrivers(t *testing.T) {
	ctx := context.Background()
	drivers := twoDrivers()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockedRepo(ctrl)

	repo.EXPECT().
		GetAllDrivers(gomock.Any(), gomock.Any(), gomock.Any()).
		Times(1).
		Return(drivers, nil)

	serviceMy := locationsvc.New(repo)
	drivers_answer, err := serviceMy.GetAllDrivers(ctx, 1, nil)

	require.NoError(t, err)
	require.Equal(t, drivers, drivers_answer)
}

func TestChangeOneDriverDataNotExist(t *testing.T) {
	ctx := context.Background()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockedRepo(ctrl)
	serviceMy := locationsvc.New(repo)

	latLng_not_exist := &model.LatLngLiteral{Lat: 100, Lng: 100}
	err := serviceMy.ChangeOneDriverData(ctx, "ID_1", latLng_not_exist)

	require.Equal(t, err.Error(), service.ErrIncorectLocation.Error())
}

func TestChangeOneDriverExist(t *testing.T) {
	ctx := context.Background()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockedRepo(ctrl)
	serviceMy := locationsvc.New(repo)

	repo.EXPECT().
		WithNewTx(gomock.Any(), gomock.Any(), gomock.Any()).
		Times(1).
		Return(nil)

	latLng_exist := &model.LatLngLiteral{Lat: 2, Lng: 100}
	err := serviceMy.ChangeOneDriverData(ctx, "ID_1", latLng_exist)

	require.NoError(t, err)
}
