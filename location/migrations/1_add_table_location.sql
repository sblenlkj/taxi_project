-- +goose Up
-- +goose StatementBegin
CREATE TABLE locations
(
    driver_id     varchar     PRIMARY KEY NOT NULL,
    driver_name   varchar     NOT NULL,
    driver_auto   varchar     NOT NULL,
    lat           float       NOT NULL,
    lng           float       NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE locations;
-- +goose StatementEnd