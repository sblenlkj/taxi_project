-- +goose Up
-- +goose StatementBegin
INSERT INTO locations (driver_id, driver_name, driver_auto, lat, lng)
values ('d4e111c4-a570-4cc0-afa5-7937b1d757b5', 'John', 'A', 50.0, 55.0),
       ('d9e70a75-9a92-4147-b09d-b9f97160bf62', 'Paul','A',  51.0, 50.0),
       ('1fd7d876-0acb-4cbd-8dce-8696f12f6217', 'George', 'D', 56.0, 57.0);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
TRUNCATE locations RESTART IDENTITY CASCADE;
-- +goose StatementEnd